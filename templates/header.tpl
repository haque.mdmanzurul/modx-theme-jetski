<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>M2M</title>

    <!-- Bootstrap -->
    <link href="[[++site_url]]theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="[[++site_url]]theme/css/style.css" rel="stylesheet">
    <link href="[[++site_url]]theme/css/responsive.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <header>
  	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
        	<div class="navbar-header">
               <a href="#" class="brand navbar-brand"><img alt="Logo" src="[[++site_url]]/theme/images/logo.png" class="img-responsive"></a>
                <button id="navbar-toggle-id" data-target=".collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
               </button>
            </div>
            
            <div id="m2m-nav" class="collapse navbar-collapse">
            	<ul class="nav">
                    <li class="first active" id="item-1"><a title="Title" href="#">Industries & Solutions <span class="square"></span></a></li>
                    <li class="" id="item-1"><a title="Title" href="#">Products <span class="square"></span></a></li>
                    <li class="first active" id="item-1"><a title="Title" href="#">Software <span class="square"></span></a></li>
                    <li class="first active" id="item-1"><a title="Title" href="#">Company <span class="square"></span></a></li>
                    <li class="first active" id="item-1"><a title="Title" href="#">Media <span class="square"></span></a></li>
                    <li class="last" id="item-1"><a title="Title" href="#">Contact</a></li>
                </ul>
            </div>
        </nav>    	
    </div>
  </header>