<section class="home-banner">
    <img class="img-responsive" src="[[+site_url]]theme/images/home_banner.jpg" />	
    <div class="container video-banner">
      <div class="row">
      	 <div class="col-md-7">
         	<h1><span>SuccorfishM2M…</span>Putting you in control</h1>
            <img class="img-responsive" src="[[+site_url]]theme/images/video_banner.png" />
         </div>
      </div>	
    </div>
</section>