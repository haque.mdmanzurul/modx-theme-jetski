<section class="our-partners">
   <div class="container">
                <div class="row">
                    <div class="container"><h3>Our Partners</h3></div>
                </div>
   		<div class="row">
        	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
              </ol>
            
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                <img class="img-responsive" src="[[++site_url]]/theme/images/bbc.png" />
                <img class="img-responsive" src="[[++site_url]]/theme/images/channel5.png" />
                <img class="img-responsive" src="[[++site_url]]/theme/images/nationaloceangraphy.png" />
                </div>
                <div class="item">
                <img class="img-responsive" src="[[++site_url]]/theme/images/hydrosphere.png" />
                <img class="img-responsive" src="[[++site_url]]/theme/images/subsea7.png" />
                </div>
              </div>
            
              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>
        </div>
   </div>
</section>
