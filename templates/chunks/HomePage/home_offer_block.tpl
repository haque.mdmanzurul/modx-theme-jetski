<section class="home-offer" style="background: url([[+tv.offers_background]]) no-repeat top left; background-size: 100% 100%">
<div class="container">
<div class="row">
<div class="col-md-6 [[!If? &subject=´[[+idx:mod=´2´]]´ &operator=´EQ´ &operand=´0´ &then=col-md-offset-6 col-sm-offset-6]]">
  <div class="offer-single [[!If? &subject=´[[+idx:mod=´2´]]´ &operator=´EQ´ &operand=´0´ &then=block-left&else=block-right]]">
    <h3>[[+pagetitle]]</h3>
    <div class="offer-single-inner"> 
     [[+introtext]]
     <br/><br/>
     <a class="readmore" href="[[~[[+id]]]]">read more</a>
    </div>
  </div>
</div>
</div>
</section>