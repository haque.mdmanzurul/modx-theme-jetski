[[$header]]
[[$home_banner]]
<section class="heading-content">
	<div class="container">
    	<div class="row">
        	<div class="col-md-12">
            	<h2>[[*intro_content_title]]</h2>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-6 col-sm-6">
				<div class="content-two-column-left">[[*intro_content_left]]</div>
             </div> 
            <div class="col-md-6 col-sm-6">    
				<div class="content-two-column-right">[[*intro_content_right]]</div>                            
            </div>
        </div>
    </div>
</section>

<section class="service-content">
	<div class="container">
    	    [[getResources?&parents=`2`&tpl=`home_service_block`&showHidden=`1`&limit=`4`&includeTVs=`1`&processTVs=`1`&includeContent=´1´&idx=´1´]]
         
        </div>
</section>
[[getResources?&parents=`7`&tpl=`home_offers_block`&showHidden=`1`&limit=`3`&sortby=`{"publishedon":"ASC","createdon":"DESC"}`&includeTVs=`1`&processTVs=`1`&includeContent=´1´&idx=´1´]]
[[$home_our_partner]]
[[$footer]]
