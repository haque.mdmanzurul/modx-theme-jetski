<footer>
   <div class="container">
   		<div class="row">
    	<div class="col-md-3 col-sm-6">
        	<h3>Legal</h3>
            <ul class="footer_nav">
            	<li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms & Conditions</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-6">
            <h3>News</h3>
            <ul class="footer_nav">
            	<li><a href="#">System Status</a></li>
                <li><a href="#">News</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-6">
            <h3>Resources</h3>
            <ul class="footer_nav">
            	<li><a href="#">Downloads</a></li>
                <li><a href="#">FAQ</a></li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-6">
        	<h3>Contact Details</h3>
            <div class="contact_info">
            <span>+44 (0) 191 447 6883</span><br/>
            <span>mail@gmail.com</span>
            </div>
        </div>
    </div>
    
    	<div class="row">
            <div class="col-md-12"> 
                <div class="copyright_info">
                    <span>Succorfish © Copyright 2014. All Rights Reserved</span>  
                </div>
            </div>
        </div>
   </div>
</footer>    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="[[++site_url]]theme/js/bootstrap.min.js"></script>
  </body>
</html>
